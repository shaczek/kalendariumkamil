package eu.tplodzi.kalendariumtp.decorator

import android.graphics.drawable.Drawable
import android.graphics.drawable.ShapeDrawable

import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade
import java.util.HashSet

class DayDecorator(private val color: Int, private val background: Drawable, dates: Collection<CalendarDay>) : DayViewDecorator {
    private val dates: HashSet<CalendarDay> = HashSet(dates)

    override fun shouldDecorate(day: CalendarDay): Boolean {
        return dates.contains(day)
    }

    override fun decorate(view: DayViewFacade) {
//        view.addSpan(BackgroundColorSpan(color))
        val oval = ShapeDrawable()
//        oval.paint.color = color
        view.setBackgroundDrawable(background)
    }
}
