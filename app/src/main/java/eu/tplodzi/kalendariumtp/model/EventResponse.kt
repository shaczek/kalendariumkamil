package eu.tplodzi.kalendariumtp.model

data class EventResponse(val event: List<Event>)