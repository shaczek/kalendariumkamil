package eu.tplodzi.kalendariumtp.model

import com.prolificinteractive.materialcalendarview.CalendarDay

data class Event(
        val id: Int,
        val date: String,
        val name: String,
        val description: String,
        val calendarDay: CalendarDay) {
}