package eu.tplodzi.kalendariumtp

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.TextView
import android.widget.Toast
import com.inverce.mod.core.IM
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import eu.tplodzi.kalendariumtp.decorator.DayDecorator
import eu.tplodzi.kalendariumtp.model.Event
import eu.tplodzi.kalendariumtp.model.EventResponse
import eu.tplodzi.kalendariumtp.rest.rest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.view.WindowManager
import android.os.Build
import android.text.Spannable
import android.text.SpannableStringBuilder
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener

class MainActivity : AppCompatActivity(), Callback<EventResponse>, OnDateSelectedListener, OnMonthChangedListener {

    companion object {
        @JvmStatic
        private val sdf = "dd.MM.yyyy"
    }

    private lateinit var name: TextView
    private lateinit var desc: TextView
    private lateinit var calendarView: MaterialCalendarView
    private val calendarDays: MutableList<CalendarDay> = mutableListOf()
    private lateinit var eventsList: List<Event>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViews()
        rest.posts().enqueue(this)
        changeStatusBarColor()
    }

    private fun findViews() {
        name = findViewById(R.id.name)
        desc = findViewById(R.id.desc)
        calendarView = findViewById(R.id.calendar)
    }

    private fun setDates(events: List<Event>) {
        val calendar = Calendar.getInstance()
        for (event in events) {
            calendar.time = SimpleDateFormat(sdf).parse(event.date)
            calendarDays.add(CalendarDay.from(calendar.time))
        }
        calendarView.addDecorator(DayDecorator(ContextCompat.getColor(IM.context(), R.color.white), ContextCompat.getDrawable(IM.context(), R.drawable.day_circle)!!, calendarDays))
        calendarView.setOnDateChangedListener(this)
        calendarView.setOnMonthChangedListener(this)
    }

    fun boldText(fullText: String, startText: String, metaText: String) {
        val finalSpan = SpannableStringBuilder(fullText)

        if (fullText.contains("start:")) {
            val start = fullText.indexOf(startText)
            if (start == -1) {
                return
            }
            val end = start + startText.length

            finalSpan.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        if (fullText.contains("meta:")) {
            val start = fullText.indexOf(metaText)
            if (start == -1) {
                return
            }
            val end = start + metaText.length

            finalSpan.setSpan(android.text.style.StyleSpan(android.graphics.Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        desc.text = finalSpan
    }


    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = window
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.GRAY
        }
    }

    override fun onMonthChanged(widget: MaterialCalendarView, date: CalendarDay) {
        name.text = resources.getText(R.string.select_date)
        desc.text = resources.getText(R.string.default_desc)
    }

    override fun onDateSelected(widget: MaterialCalendarView, date: CalendarDay, selected: Boolean) {
        val selectedEvent = eventsList.find { it.date == SimpleDateFormat(sdf, Locale.getDefault()).format(date.date.time) }
        name.text = selectedEvent?.name ?: resources.getText(R.string.select_date)
        desc.text = selectedEvent?.description ?: resources.getText(R.string.default_desc)
        selectedEvent?.description?.let {
            boldText(selectedEvent.description, "start:", metaText = "meta:")

        }
    }

    override fun onResponse(call: Call<EventResponse>?, response: Response<EventResponse>?) {
        eventsList = ArrayList()
        response?.body()?.event?.let {
            setDates(it)
            eventsList = it
        }
    }

    override fun onFailure(call: Call<EventResponse>?, t: Throwable?) {
        Toast.makeText(IM.context(), "Błąd przy pobieraniu danych. Sprawdź połączenie internetowe", Toast.LENGTH_SHORT).show()
    }
}
