package eu.tplodzi.kalendariumtp.rest

import com.google.gson.Gson
import eu.tplodzi.kalendariumtp.model.EventResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

val rest: RestInterface by lazy {
    Retrofit.Builder()
            .baseUrl("http://www.tplodzi.eu/")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
            .create(RestInterface::class.java)
}

interface RestInterface {
    @GET("test.json")
    fun posts() : Call<EventResponse>
}
